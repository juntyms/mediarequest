<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',['as'=>'index','uses'=>'DashboardController@index']);

Route::get('login', ['as'=>'login','uses'=>'UserController@login']);
Route::post('postlogin', ['as'=>'postlogin', 'uses'=>'UserController@postlogin']);
Route::get('logout',['as'=>'logout','uses'=>'UserController@logout']);

Route::get('types', ['as'=>'type.index', 'uses'=>'TypeController@index']);
Route::get('type/add', ['as'=>'type.add', 'uses'=>'TypeController@add']);
Route::post('type/save', ['as'=>'type.save', 'uses'=>'TypeController@save']);
Route::get('type/{id}/edit', ['as'=>'type.edit', 'uses'=>'TypeController@edit']);
Route::post('type/{id}/update', ['as'=>'type.update','uses'=>'TypeController@update']);

Route::get('approvers', ['as'=>'approver.index', 'uses'=>'ApproverController@index']);
Route::get('approver/add', ['as'=>'approver.add', 'uses'=>'ApproverController@add']);
Route::post('approver/save', ['as'=>'approver.save', 'uses'=>'ApproverController@save']);
Route::get('approver/{id}/edit', ['as'=>'approver.edit','uses'=>'ApproverController@edit']);
Route::post('approver/{id}/update', ['as'=>'approver.update', 'uses'=>'ApproverController@update']);
Route::get('approver/{id}/enable/{pid}/{val}', ['as'=>'approver.enable', 'uses'=>'ApproverController@enable']);

Route::get('seq/add', ['as'=>'seq.add', 'uses'=>'SequenceController@add']);
Route::post('seq/save', ['as'=>'seq.save', 'uses'=>'SequenceController@save']);
//TODO: seq/edit
//TODO: seq/update

Route::get('assignments',['as'=>'assignment.index','uses'=>'AssignmentController@index']);
Route::post('assignment/add', ['as'=>'assignment.add', 'uses'=>'AssignmentController@add']);
Route::post('assignment/save', ['as'=>'assignment.save', 'uses'=>'AssignmentController@save']);
Route::get('assignment/{id}/delete', ['as'=>'assignment.delete', 'uses'=>'AssignmentController@delete']);

Route::get('stats', ['as'=>'stat.index', 'uses'=>'StatusController@index']);
Route::get('stat/add', ['as'=>'stat.add', 'uses'=>'StatusController@add']);
Route::post('stat/save', ['as'=>'stat.save', 'uses'=>'StatusController@save']);
Route::get('stat/{id}/edit', ['as'=>'stat.edit', 'uses'=>'StatusController@edit']);
Route::post('stat/{id}', ['as'=>'stat.update', 'uses'=>'StatusController@update']);

Route::get('posts', ['as'=>'post.index', 'uses'=>'PostController@index']);
Route::get('post/new',['as'=>'post.new', 'uses'=>'PostController@new']);
Route::post('post/save', ['as'=>'post.save', 'uses'=>'PostController@save']);
Route::get('post/file/{id}/delete',['as'=>'post.deletefile', 'uses'=>'PostController@deletefile']);
Route::post('post/{id}/addfile', ['as'=>'post.addfile', 'uses'=>'PostController@addfile']);
Route::post('post/{id}/status', ['as'=>'post.status', 'uses'=>'PostController@status']);
Route::get('post/{id}/edit', ['as'=>'post.edit', 'uses'=>'PostController@edit']);
Route::post('post/{id}/edit', ['as'=>'post.update', 'uses'=>'PostController@update']);
Route::post('post/{id}/verify', ['as'=>'post.verify', 'uses'=>'PostController@verify']);
Route::get('post/{assignment_id}/delete', ['as'=>'post.deleteproofreader', 'uses'=>'PostController@deleteproofreader']);
Route::post('post/{post_id}/assignproofreader/{approval_id}', ['as'=>'post.assignproofreader', 'uses'=>'PostController@assignproofreader']);
//approve and reject post route
Route::get('post/{post_id}/approval/{approval_id}', ['as'=>'post.approval', 'uses'=>'PostController@approval']);
Route::post('post/{post_id}/reject/{approval_id}', ['as'=>'post.reject', 'uses'=>'PostController@reject']);

//TODO: report
Route::get('reports',['as'=>'report.index','uses'=>'ReportController@index']);
Route::post('reports', ['as'=>'report.show', 'uses'=>'ReportController@show']);

Route::get('menus',['as'=>'menu.index','uses'=>'MenuController@index']);
Route::get('menu/new', ['as'=>'menu.add', 'uses'=>'MenuController@add']);
Route::post('menu/save', ['as'=>'menu.save', 'uses'=>'MenuController@save']);
Route::get('menu/assign',['as'=>'menu.assign', 'uses'=>'MenuController@assign']);
Route::post('menu/assign', ['as'=>'menu.saveassign', 'uses'=>'MenuController@saveassign']);
Route::get('menu/user', ['as'=>'menu.user','uses'=>'MenuController@user']);
Route::get('menu/{id}/delete', ['as'=>'menu.delete', 'uses'=>'MenuController@delete']);