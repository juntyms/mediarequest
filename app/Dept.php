<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dept extends Model
{
    protected $table = 'depts';    

    public function seqs()
    {
        return $this->hasMany('App\ReqMediaApproverSeq','dept_id','id');
    }
}
