<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $email_message;
    public $subject;

    public function __construct($email_message, $subject)
    {
        $this->email_message = $email_message;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@sct.edu.om')
                    ->view('mail.postnotification')
                    ->subject($this->subject)
                    ->with(['email_message'=> $this->email_message]);
    }
}
