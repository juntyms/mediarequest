<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaMenu extends Model
{
    protected $table = 'req_media_menus';
    
    protected $fillable = ['parent_id','menu','url','icon'];
    
    public function parent() 
    {
        return $this->belongsToOne(static::class, 'parent_id');
    }

    //each category might have multiple children
    public function children() 
    {
        return $this->hasMany(static::class, 'parent_id')->orderBy('menu', 'asc');
    }
}
