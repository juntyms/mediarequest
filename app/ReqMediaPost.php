<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaPost extends Model
{
    protected $table = 'req_media_posts';

    protected $fillable = ['title','content','req_media_ay_id','req_media_status_id','status_count','req_media_post_type_id','requested_by'];

    public function status()
    {
        return $this->hasOne('App\ReqMediaStatus','id','req_media_status_id');
    }

    public function posttype()
    {
        return $this->hasOne('App\ReqMediaPostType','id','req_media_post_type_id');
    }

    public function files()
    {
        return $this->hasMany('App\ReqMediaPostFile','req_media_post_id','id');
    }

    public function requester()
    {
        return $this->hasOne('App\User','id','requested_by');
    }
}
