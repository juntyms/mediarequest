<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaApproverAssignment extends Model
{
    protected $table = 'req_media_approver_assignments';

    protected $fillable = ['req_media_approver_seq_id','user_id'];

    public function approverseq()
    {
        return $this->hasOne('App\ReqMediaApproverSeq','id','req_media_approver_seq_id')->orderBy('seq','asc');
    }

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}
