<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaApproverSeq extends Model
{
    protected $table = 'req_media_approver_seqs';

    protected $fillable = ['req_media_approver_id','seq','req_media_post_type_id','dept_id'];

    public function department()
    {
        return $this->hasOne('App\Dept','id','dept_id');
    }

    public function approver()
    {
        return $this->hasOne('App\ReqMediaApprover','id','req_media_approver_id');
    }

    public function posttype()
    {
        return $this->hasOne('App\ReqMediaPostType','id','req_media_post_type_id');
    }

    public function postapprovals()
    {
        return $this->hasMany('App\ReqMediaPostApproval','req_media_approver_seq_id','id');
    }

    public function assignments()
    {
        return $this->hasMany('App\ReqMediaApproverAssignment','req_media_approver_seq_id','id');
    }
}
