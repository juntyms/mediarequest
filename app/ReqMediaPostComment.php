<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaPostComment extends Model
{
    protected $table = 'req_media_post_comments';

    protected $fillable = ['req_media_post_approval_id','comment','by'];
}
