<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaPostFile extends Model
{
    protected $table = 'req_media_post_files';

    protected $fillable = ['req_media_post_id','file_path'];
}
