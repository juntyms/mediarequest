<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaStatus extends Model
{
    protected $table = 'req_media_status';

    protected $fillable = ['description'];
}
