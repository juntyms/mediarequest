<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaUserMenu extends Model
{
    protected $table = 'req_media_user_menu';

    protected $fillable = ['req_media_menu_id','user_id'];

    public function users()
    {
        return $this->hasMany('App\User','id','user_id');
    }

    public function menu()
    {
        return $this->hasOne('App\ReqMediaMenu','id','req_media_menu_id');
    }
}
