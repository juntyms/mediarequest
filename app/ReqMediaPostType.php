<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaPostType extends Model
{
    protected $table = 'req_media_post_types';

    protected $fillable = ['post_type'];
}
