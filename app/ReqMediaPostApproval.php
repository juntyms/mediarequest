<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaPostApproval extends Model
{
    protected $table = 'req_media_post_approvals';

    protected $fillable = ['req_media_post_id','req_media_approver_seq_id','approver_id','date_approved','comment','req_media_status_id'];

    public function post()
    {
        return $this->hasOne('App\ReqMediaPost','id','req_media_post_id');
    }

    public function approverseq()
    {
        return $this->hasOne('App\ReqMediaApproverSeq','id','req_media_approver_seq_id');
    }

    public function status()
    {
        return $this->hasOne('App\ReqMediaStatus','id','req_media_status_id');
    }
}
