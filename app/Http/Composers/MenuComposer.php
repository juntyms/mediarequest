<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\ReqMediaUserMenu;
use Auth;

class MenuComposer {

  public function nav(View $view)
  {
      $usermenus = ReqMediaUserMenu::where('user_id',Auth::user()->id)->get();

      $view->with('usermenus',$usermenus);
  }

}
