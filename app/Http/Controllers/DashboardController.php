<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ReqMediaApproverAssignment;
use Auth;
use App\ReqMediaPost;
use App\ReqMediaApprover;
use App\ReqMediaApproverSeq;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // Display All Request
        $reqs = ReqMediaPost::where('requested_by',Auth::user()->id)
                            ->orderBy('created_at','desc')
                            ->get();

        // Check Privilege and display only requests based on privileges
        $approver_assignments = ReqMediaApproverAssignment::with(['approverseq'=> function($query) {
                                        $query->orderBy('req_media_approver_seqs.seq','asc');
                                    }])
                                    ->where('user_id', Auth::user()->id)                                    
                                    ->get();        
        //dd($approver_assignments);
       // $approver_assignments = [];
       \Debugbar::info($approver_assignments);

    /*
       $proofreaders = \DB::table('req_media_approvers')
                            ->leftjoin('req_media_approver_seqs','req_media_approver_seqs.req_media_approver_id','=','req_media_approvers.id')
                            ->leftjoin('req_media_approver_assignments','req_media_approver_assignments.req_media_approver_seq_id','=','req_media_approver_seqs.id')
                            ->leftjoin('users','users.id','=','req_media_approver_assignments.user_id')
                            ->where('req_media_approvers.id','=',1)
                            ->where('users.dept_id','=',Auth::user()->dept_id)
                            ->select('users.fullname','users.id','req_media_approver_assignments.id as assign_id')
                            ->get(); */

        $proofreaders = ReqMediaApproverSeq::where('dept_id',Auth::user()->dept_id)
                                            ->where('req_media_approver_id',1)
                                            ->get();

        $users = \DB::table('users')
                    ->where('isStaff',1)
                    ->where('hr_employee_status_id',1)
                    ->where('dept_id',Auth::user()->dept_id)
                    ->pluck('fullname','id');
       
        return view('user.index')
                ->with('approver_assignments',$approver_assignments)
                ->with('proofreaders',$proofreaders)
                ->with('reqs',$reqs)
                ->with('users',$users);
    }
}
