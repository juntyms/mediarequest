<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Mail\PostNotification;

use App\ReqMediaPost;
use App\ReqMediaPostFile;
use App\ReqMediaPostType;
use App\ReqMediaPostApproval;
use App\ReqMediaPostApprovalDetail;
use App\ReqMediaApproverSeq;
use App\ReqMediaApproverAssignment;
use App\ReqMediaPostComment;
use App\Http\Requests\PostRequest;
use Auth;
use Alert;
use Purifier;
use App\User;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $reqs = ReqMediaPost::where('requested_by',Auth::user()->id)
                            ->orderBy('created_at','desc')
                            ->get();

        return view('post.index')
                    ->with('reqs',$reqs);
    }

    public function new()
    {
        $reqtypes = ReqMediaPostType::pluck('post_type','id');

        return view('post.new')->with('reqtypes',$reqtypes);
    }

    public function save(PostRequest $request)
    {
        //Check/get Approver Sequence 
        $app_seq = \DB::table('req_media_approver_seqs')
                        ->where('req_media_post_type_id',$request->req_media_post_type_id)
                        ->where('dept_id', Auth::user()->dept_id)
                        ->orderBy('seq','asc')
                        ->get();                        

        if (!$app_seq->isEmpty()) { // Making sure approvers exist
        
            $request['req_media_status_id'] = 1;
            
            $request['requested_by'] = Auth::user()->id;
            
            // get current AY
            $ay =  \DB::table('req_media_ays')->where('active',1)->first();

            $request['req_media_ay_id'] = $ay->id;

            $request['content'] = Purifier::clean($request->content);

            //save to req_media_posts table
            $post_id =  ReqMediaPost::create($request->all());

            $path = $request->file('image')->store('public');
                        
            Storage::setVisibility($path, 'public');            

            $postfile = new ReqMediaPostFile;

                $postfile->req_media_post_id = $post_id->id;

                $postfile->file_path = $path;

            $postfile->save();        

            //save to req_media_post_approval table
            
            $approver = -1;

            $seqcnt = 0;

            foreach($app_seq as $seq) 
            {   
                if ($seqcnt > 0) {
                    $approver = 0;
                }

                $seqcnt++;

                $post_approval = new ReqMediaPostApproval;

                $post_approval->req_media_post_id =  $post_id->id;
                $post_approval->req_media_approver_seq_id = $seq->id;
                $post_approval->approver_id =$approver;
                $post_approval->req_media_status_id = 2; //Processing Status = 2

                $post_approval->save();
            }

            flash('Request Saved as draft make sure to finalize this request')->success();

            return redirect()->route('post.index');
        
        } else {

            flash('Unable to find Approvers, Contact ETC')->error();

            return redirect()->route('post.new')->withInput();
        }
        
    }

    public function addfile(Request $request, $id)
    {        

        $path = $request->file('image')->store('public');
                        
        Storage::setVisibility($path, 'public');    

        $postfile = new ReqMediaPostFile;

            $postfile->req_media_post_id = $id;

            $postfile->file_path = $path;

        $postfile->save(); 

        Alert::success('Add File', 'Successfully Added File');

        return redirect()->route('post.index');

    }

    public function deletefile($id)
    {
        // Search File
        $file = ReqMediaPostFile::findOrFail($id);
        // Delete file from storage
        Storage::disk('local')->delete($file->file_path);
        // Delete from database
        ReqMediaPostFile::destroy($id);        

        // Sweet Alert
        Alert::error('Delete', 'File Deleted');

        return redirect()->route('post.index');
    }

    public function status($id, Request $request) // Submit Post
    {
        $post = ReqMediaPost::findOrFail($id);

        $post->req_media_status_id = $request->status; // Set Status to processing

        $post->save();
        
        // Get the approver seq id
        $req_media_post_approval = \DB::table('req_media_post_approvals')
                                        ->where('req_media_post_id',$id)
                                        ->where('approver_id','-1')
                                        ->first();

        // query approvers id
        $approver_assignments = \DB::table('req_media_approver_assignments')
                            ->where('req_media_approver_seq_id',$req_media_post_approval->req_media_approver_seq_id)
                            ->select('user_id')
                            ->pluck('user_id');

        $email_message = "New Post Request require your attention";

        $approvers = \DB::table('users')
                        ->whereIn('id',$approver_assignments)
                        ->pluck('email');

        Mail::to($approvers)
            ->bcc('support@sct.edu.om')                    
            ->send(new PostNotification($email_message,'Post Needs Approval'));    
        

        return redirect()->route('post.index');
    }

    public function edit($id)
    {
        $post = ReqMediaPost::findOrFail($id);

        $reqtypes = ReqMediaPostType::pluck('post_type','id');

        return view('post.edit')
                ->with('reqtypes',$reqtypes)
                ->with('post',$post);
    }

    public function update($id, Request $request) //TODO: create a Request Validation
    {
        $post = ReqMediaPost::findOrFail($id);

        $post->content = $request->content;

        $post->title = $request->title;

        $post->save();

        return redirect()->route('post.index');
    }

    public function verify($id, Request $request) //TODO: Create a Request Validation the same as update method
    {
        $post = ReqMediaPost::findOrFail($id);

        $post->content = $request->content;

        $post->title = $request->title;

        $post->save();

        Alert::success('Update Successful', 'Article Verified');

        return redirect()->route('index');
    }

    public function approval($post_id, $approval_id) 
    {
        /**
         * 1. Query req_media_post_approval where id and approver_id = -1
         * 2. Update approver id to auth::user()
         * 3. Query req_media_post_approval where req_media_post_id and approver_id = 0 order by id get first record
         * 4. update to -1          
         */
        ReqMediaPostApproval::where('id',$approval_id)
                            ->update(['approver_id'=> Auth::user()->id, 
                                    'comment'=>'(NULL)',
                                    'req_media_status_id'=>3]);

        //Add to approval details table
        ReqMediaPostApprovalDetail::create(['req_media_post_approval_id'=>$approval_id,
                                            'approver_id'=>Auth::user()->id
                                            ]);

        $approval = \DB::table('req_media_post_approvals')
                        ->where('req_media_post_id',$post_id)
                        ->where('approver_id',0)
                        ->orderBy('id','asc')
                        ->first();

        if ($approval) {
            \DB::table('req_media_post_approvals')
                ->where('id',$approval->id)
                ->update(['approver_id'=>'-1']);                    

            //send email notification to next approver
            $get_seq_id = \DB::table('req_media_post_approvals')
                            ->where('id',$approval->id)
                            ->first();

            $approvers_assigned_query = \DB::table('req_media_approver_assignments')
                                    ->where('req_media_approver_seq_id',$get_seq_id->req_media_approver_seq_id)
                                    ->select('user_id')
                                    ->get();

            $approvers_assigned = $approvers_assigned_query->pluck('user_id');

            $approvers_query = \DB::table('users')
                            ->select('email')
                            ->whereIn('id',$approvers_assigned)
                            ->get();

            $approvers = collect($approvers_query)->flatten()->all();

            $email_message = "Please review this post for approval";            

            Mail::to($approvers)
                    ->bcc('support@sct.edu.om')                    
                    ->send(new PostNotification($email_message,'Post Needs Approval'));            

            Alert::success('Approval','Approved');
        } else {
            // Last Approver
            //Update req_media_status_id from req_media_posts
            ReqMediaPost::where('id',$post_id)
                            ->update(['req_media_status_id'=>'5']); // Status = 5 --> Posted
        
            $email_message = "Your Post has been posted";

            $post = ReqMediaPost::findOrFail($post_id);

            $requester = User::findorFail($post->requested_by);

            Mail::to($requester->email)
                    ->bcc('support@sct.edu.om')                    
                    ->send(new PostNotification($email_message,'Request Status'));


            Alert::success('Approval','Completed');
        }
                             
        return redirect()->route('index');

    }

    public function reject($post_id, $approval_id, Request $request) //TODO add validation request
    {
        //Update the current approver back to 0
        ReqMediaPostApproval::where('id',$approval_id)                                
                                ->update(['approver_id'=>'0']);

        //get the previous approver from the table and update the comment field
        $rejected = \DB::table('req_media_post_approvals')
                        ->where('req_media_post_id',$post_id)
                        ->where('approver_id','>',0)
                        ->orderby('id','desc')
                        ->first();
        
        if ($rejected) {
            // get the previous approvers id
            $prev_approver_id = $rejected->approver_id;

            //Update the Previous approver details
            ReqMediaPostApproval::where('id',$rejected->id)
                               ->update(['approver_id'=>'-1',
                                        'comment'=>$request->comment,
                                        'req_media_status_id'=>'4']);

            //add the comment to the comments table
            ReqMediaPostComment::create(['req_media_post_approval_id'=>$rejected->id,
                                        'comment'=>$request->comment,
                                        'by'=>Auth::user()->id]);     
                                                    
            $email_message = "Previously Approved request has been rejected";            
            
        } else {
            // Already on the first record
            ReqMediaPostApproval::where('id',$approval_id)                                
                                ->update(['approver_id'=>'-1']);

            ReqMediaPost::where('id',$post_id)
                                ->update(['req_media_status_id'=>'4']);

            $mediapost = ReqMediaPost::find($post_id);

            $email_message = "Post Request has been rejected";

            $prev_approver_id = $mediapost->requested_by;
            
        }

        // Query the Previous approver details which will be used for sending email
        $prev_approver = \DB::table('users')
                        ->where('id',$prev_approver_id)
                        ->first();

        Mail::to($prev_approver->email)
                    ->bcc('support@sct.edu.om')                    
                    ->send(new PostNotification($email_message,'Post Rejected'));

        Alert::error('Rejected', 'Post Rejected');

        return redirect()->route('index');        
        
    }

    public function assignproofreader($post_id, $approver_id, Request $request)
    {
        // Delete any proofreader currently assigned to the post type
        $post = ReqMediaPost::findOrFail($post_id);

        $seq = ReqMediaApproverSeq::where('req_media_post_type_id',$post->req_media_post_type_id)
                                    ->where('req_media_approver_id', 1)
                                    ->where('dept_id', Auth::user()->dept_id)
                                    ->first();

        // Approve the post to send to proofreader
        ReqMediaApproverAssignment::create(['req_media_approver_seq_id' => $seq->id, 'user_id' =>$request->proofreader]);

        $this->approval($post_id, $approver_id);        

        Alert::success('Sent Request','Request Sent to Proofreader');

        return redirect()->route('index');      
    }

    public function deleteproofreader($assignment_id)
    {
        $assignment = ReqMediaApproverAssignment::findOrFail($assignment_id);

        $assignment->delete();

        Alert::error('Deleted','Proof Reader Deleted');

        return redirect()->route('index');
    }

}
