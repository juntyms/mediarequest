<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReqMediaUserMenu;
use App\ReqMediaMenu;
use App\User;


class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $menus = ReqMediaMenu::all();

        return view('menu.index')->with('menus',$menus);
    }

    public function add()
    {
        $parent_menus = \DB::table('req_media_menus')
                            ->where('parent_id',0)
                            ->pluck('menu','id');

        return view('menu.add')->with('parent_menus',$parent_menus);
    }

    public function save(Request $request)
    {
        if (empty($request->parent_id))
        {
            $request['parent_id'] = 0;
        }        

        ReqMediaMenu::create($request->all());

        flash('Menu Added')->success();
        
        return redirect()->route('menu.index');
    }

    public function assign()
    {
        
        $menus = \DB::table('req_media_menus')                        
                    ->pluck('menu','id');
                    

        $users = \DB::table('users')
                    ->select(\DB::Raw("concat(users.fullname,'-',users.email) as completename"),'id')
                    ->where('isStaff','=',1)
                    ->pluck('completename','id');

        return view('menu.assign')
                ->with('menus',$menus)
                ->with('users',$users);
    }

    public function saveassign(Request $request)
    {
        $assign_check = \DB::table('req_media_user_menu')
                            ->where('req_media_menu_id','=',$request->req_media_menu_id)
                            ->where('user_id','=',$request->user_id)
                            ->first();

        if (empty($assign_check))
        {
            ReqMediaUserMenu::create($request->all());

            flash('User Menu Saved')->success();
        } else {
            flash('User Menu Already Assigned')->error();
        }

        return redirect()->route('menu.user');
    }

    public function user()
    {
        $users = User::has('menus')->get();        

        return view('menu.user')
                ->with('users',$users);
    }

    public function delete($id)
    {
        $menu = ReqMediaUserMenu::findOrFail($id);

        $menu->delete();

        flash('Menu assignment deleted')->error();

        return redirect()->route('menu.user');
    }

}
