<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Dept;
use App\ReqMediaApproverAssignment;


class AssignmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $depts = Dept::pluck('name','id');

        $approver_assignments = ReqMediaApproverAssignment::all();

        return view('assignment.index')
                    ->with('depts',$depts)
                    ->with('approver_assignments',$approver_assignments);
    }

    public function add(Request $request)
    {
        $users = User::select(\DB::raw("CONCAT(email,'-',fullname) as userfullname"),'id')             
                        ->where('hr_employee_status_id',1)           
                        ->where('isStaff',1)
                        ->pluck('userfullname','id');

        $approver_types = \DB::table('req_media_approver_seqs')
                            ->leftjoin('req_media_approvers','req_media_approvers.id','=','req_media_approver_seqs.req_media_approver_id')
                            ->leftjoin('req_media_post_types','req_media_post_types.id','=','req_media_approver_seqs.req_media_post_type_id')
                            ->select(\DB::raw("CONCAT(req_media_approvers.approver,'-',req_media_post_types.post_type) as approvertype"),'req_media_approver_seqs.id')
                            ->where('req_media_approver_seqs.dept_id','=',$request->deptid)
                            ->pluck('approvertype','req_media_approver_seqs.id');

        $department = Dept::findOrFail($request->deptid);

        return view('assignment.add')
                    ->with('users',$users)
                    ->with('approver_types',$approver_types)
                    ->with('department',$department);
    }

    public function save(Request $request)
    {
        $assignment = ReqMediaApproverAssignment::where('user_id',$request->user_id)
                            ->where('req_media_approver_seq_id',$request->req_media_approver_seq_id)
                            ->first();

        if ($assignment) {

            flash('Assignment Already Exist')->error();

        } else {

            flash('User Approver Assignment Successful')->success();

            ReqMediaApproverAssignment::create($request->all());
        }
        

        return redirect()->route('assignment.index');
       
    }

    public function delete($id)
    {
        $assignment = ReqMediaApproverAssignment::findOrFail($id);

        $assignment->delete();

        flash('Assignment Deleted')->error();

        return redirect()->route('assignment.index');
    }
}
