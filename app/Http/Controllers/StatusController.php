<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReqMediaStatus;

class StatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $status = ReqMediaStatus::all();

        return view('stat.index')
                    ->with('status',$status);
    }

    public function add()
    {
        return view('stat.add');
    }

    public function save(Request $request)
    {
        ReqMediaStatus::create($request->all());

        flash('Status Saved')->success();

        return redirect()->route('stat.index');
    }

    public function edit($id)
    {
        $stat = ReqMediaStatus::findOrFail($id);

        return view('stat.edit')->with('stat',$stat);
    }

    public function update(Request $request, $id)
    {
        $stat = ReqMediaStatus::findOrFail($id);

        $stat->update($request->all());

        flash('Status Updated')->success();

        return redirect()->route('stat.index');
    }
}
