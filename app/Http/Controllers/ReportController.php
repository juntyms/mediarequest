<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReqMediaPost;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $ays = \DB::table('req_media_ays')
                    ->Select(\DB::raw("concat(ay,'- semester - ', sem) as ay"),'id')
                    ->pluck('ay','id');

//        \Debugbar::info($ays);

        return view('report.index')
                ->with('ays',$ays);
    }

    public function show(Request $request)
    {
        $postRequests = ReqMediaPost::where('req_media_ay_id',$request->ay)
                                        ->get();

        return view('report.show')
                ->with('postRequests',$postRequests);
    }
}
