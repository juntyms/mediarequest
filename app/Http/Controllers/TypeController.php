<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReqMediaPostType;

class TypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $req_media_post_types = ReqMediaPostType::all();

        return view('type.index')->with('req_media_post_types',$req_media_post_types);
    }

    public function add()
    {
        $req_media_post_types = ReqMediaPostType::all();

        return view('type.add')->with('req_media_post_types',$req_media_post_types);
    }

    public function save(Request $request)
    {
        ReqMediaPostType::create(['post_type'=>$request->post_type]);

        flash('Post Type Saved')->success();

        return redirect()->route('type.add');
    }

    public function edit($id)
    {
        $post_type = ReqMediaPostType::findOrFail($id);

        $req_media_post_types = ReqMediaPostType::all();

        return view('type.edit')
                ->with('id',$id)
                ->with('req_media_post_types',$req_media_post_types)
                ->with('post_type',$post_type);
    }

    public function update(Request $request)
    {
        $req_media_post_type = ReqMediaPostType::findOrFail($request->id);

        $req_media_post_type->update($request->all());

        flash('Successfully Updated')->success();

        return redirect()->route('type.add');
    }
}
