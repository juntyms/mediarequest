<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReqMediaApprover;

class ApproverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $approvers = ReqMediaApprover::all();

        return view('approver.index')
                    ->with('approvers',$approvers);
    }

    public function add()
    {
        $approvers = ReqMediaApprover::all();

        return view('approver.add')
                ->with('approvers',$approvers);
    }

    public function save(Request $request)
    {
        ReqMediaApprover::create($request->all());

        flash('New Approver Saved')->success();

        return redirect()->route('approver.add');
    }

    public function edit($id)
    {
        $reqmediaapprover = ReqMediaApprover::findOrFail($id);

        $approvers = ReqMediaApprover::all();

        return view('approver.edit')
                ->with('reqmediaapprover',$reqmediaapprover)
                ->with('approvers',$approvers);
    }

    public function update(Request $request, $id)
    {
        $reqmediaapprover = ReqMediaApprover::findOrFail($id);

        $reqmediaapprover->update($request->all());

        flash('Approver Successfully Updated')->success();

        return redirect()->route('approver.add');
    }

    public function enable($id, $pid, $val)
    {
        switch ($pid)
        {
            case 1: // is_approved
                if ($val == 1) {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_approved'=>'0']);
                } else {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_approved'=>'1']);
                }
                break;
            case 2: // is_reject
                if ($val == 1) {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_reject'=>'0']);
                } else {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_reject'=>'1']);
                }
                break;
            case 3:
                if ($val == 1) {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_verified'=>'0']);
                } else {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_verified'=>'1']);
                }
                break;
            case 4:
                if ($val == 1) {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_edit'=>'0']);
                } else {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_edit'=>'1']);
                }
                break;
            case 5:
                if ($val == 1) {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_image'=>'0']);
                } else {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_image'=>'1']);
                }
                break;
            case 6:
                if ($val == 1) {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_comment'=>'0']);
                } else {
                    \DB::table('req_media_approvers')->where('id',$id)->update(['is_comment'=>'1']);
                }
                break;
        }

        return redirect()->route('approver.index');
    }
}
