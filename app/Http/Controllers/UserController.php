<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Adldap\Laravel\Facades\Adldap;


class UserController extends Controller
{
    
    public function login()
    {
        return view('user.login');
    }

    public function postlogin(Request $request)
    { 
        /**
         * Templogin 
         */
        if (env('TEMP_LOGIN') == true) {
            Auth::loginUsingId(env('LOGIN_ID'));
            return redirect()->route('index');
        }
         
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            return redirect()->route('index');
        } else {
            return redirect()->route('index');
        }        
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('index');
    }

}
