<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ReqMediaApprover;
use App\ReqMediaPostType;
use App\ReqMediaApproverSeq;
use App\Dept;

class SequenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add()
    {
        $approvers = ReqMediaApprover::pluck('approver','id');

        $types = ReqMediaPostType::pluck('post_type','id');

        $depts = Dept::pluck('name','id');

        /*$seqs = ReqMediaApproverSeq::orderBy('req_media_post_type_id','asc')
                                    ->orderBy('dept_id','asc')->get();
            */
        $seqs = Dept::has('seqs')->get();

        return view('sequence.add')
                    ->with('approvers',$approvers)
                    ->with('types',$types)
                    ->with('depts',$depts)
                    ->with('seqs',$seqs);
    }

    public function save(Request $request)
    {       
        $sequenceCheck = \DB::table('req_media_approver_seqs')
                                ->where('req_media_approver_id', $request->req_media_approver_id)
                                ->where('req_media_post_type_id', $request->req_media_post_type_id)
                                ->where('dept_id', $request->dept_id)
                                ->first();

        if (empty($sequenceCheck)) {        

            $seqCheck = ReqMediaApproverSeq::where('req_media_post_type_id',$request->req_media_post_type_id)
                                    ->where('dept_id',$request->dept_id)
                                    ->count();

            $request['seq'] = ++$seqCheck;

            ReqMediaApproverSeq::create($request->all());

            flash('Approver Sequence Successful')->success();            

        } else {

            // Already exist
            flash('Approve Sequence Already Exist!')->error();                    
        }

        return redirect()->route('seq.add');

        
    }

}
