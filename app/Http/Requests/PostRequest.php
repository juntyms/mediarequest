<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'req_media_post_type_id'=>'required',
            'title'=>'required',
            'content'=>'required',
            'image'=>'required|mimes:jpeg,png,bmp'
        ];
    }

    public function messages()
    {
        return [
            'req_media_post_type_id.required'=>'Please Select Media Type',
            'image.required'=>'File Not Found',
            'mimes' => ' Only jpef, png, bmp are allowed'
        ];
    }
}
