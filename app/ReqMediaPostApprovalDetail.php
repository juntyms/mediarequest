<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaPostApprovalDetail extends Model
{
    protected $table = 'req_media_post_approval_details';

    protected $fillable = ['req_media_post_approval_id','approver_id'];
}
