<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqMediaApprover extends Model
{
    protected $table = 'req_media_approvers';

    protected $fillable = ['approver'];

    /*
    public function postapprovals()
    {
        return $this->hasMany('App\ReqMediaPostApproval','req_media_approver_seq_id','id');
    } */
    public function seqs()
    {
        return $this->hasMany('App\ReqMediaApproverSeq','req_media_approver_id','id');
    }
}
