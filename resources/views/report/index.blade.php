@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>    
    <li class="breadcrumb-item active">Report</li>
</ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(['route'=>'report.show']) !!}
                    <div class="form-group">
                        {!! Form::select('ay',$ays, null, ['class'=>'form-control']) !!}
                    </div>   
                    <div class="form-group">
                        <button class="btn btn-primary"> Load</button>
                    </div> 
                    {!! Form::close() !!}
                </div>
            </div>
            
        </div>
        
        
    </div>
@endsection