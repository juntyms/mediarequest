@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>    
    <li class="breadcrumb-item active">Report</li>
</ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered"> 
                <tr class="bg-primary text-white">
                    <th>SN</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Status</th>
                    <th>Date Requested</th>
                </tr>
                @php 
                    $sn = 1
                @endphp
                @foreach($postRequests as $postrequest)
                <tr>
                    <td>{!! $sn++ !!}</td>
                    <td>{!! $postrequest->title !!}</td>
                    <td>{!! $postrequest->content !!}</td>
                    <td>{!! $postrequest->status->description !!}</td>
                    <td>{!! \Carbon\Carbon::parse($postrequest->created_at)->format('d-M-Y') !!}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection