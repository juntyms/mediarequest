<div class="form-group">
    <label for="typename" class="control-label">Name</label>
    {!! Form::text('approver',null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    <button class="btn btn-primary"> <i class="fas fa-save"></i> {!! $submitText !!}</button>
</div>