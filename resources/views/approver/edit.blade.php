@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Approver</li>
    <li class="breadcrumb-item active"> Edit</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card mb-3">
            <div class="card-header bg-info text-white">
            <i class="fas fa-user-check"></i>
            Edit Approver</div>
            <div class="card-body">
                {!! Form::model($reqmediaapprover,['route'=>['approver.update',$reqmediaapprover->id]]) !!}
                    @include('approver.form',['submitText'=>' Update'])
                {!! Form::close() !!}
            </div>            
        </div>
    </div>
</div>
@include('approver.list')
@endsection