<div class="row">
    <div class="col-lg-12">
        <div class="card mb-3 border-info">
            <div class="card-header bg-info text-white"> <i class="fas fa-user-check"></i> Approver List</div>
            <div class="card-body">
                <table class="table table-bordered">
                    <tr class="thead-light">
                        <th>SN</th>
                        <th>Approver Type</th>
                        <th>Permission</th>
                        <th>Action</th>
                    </tr>
                    @php 
                        $sn = 1;
                    @endphp
                    @foreach($approvers as $approver)
                    <tr>
                        <td>{!! $sn++ !!}</td>
                        <td>{!! $approver->approver !!}</td>
                        <td>
                            @if ($approver->is_approved == 1)
                                <a href="{!! URL::route('approver.enable',[$approver->id,1,1]) !!}" class="badge badge-success"> <i class="far fa-check-circle"></i> Approve</a>
                            @else
                                <a href="{!! URL::route('approver.enable',[$approver->id,1,0]) !!}" class="badge badge-secondary"> <i class="far fa-times-circle"></i> Approve</a>
                            @endif
                            @if ($approver->is_reject == 1)
                                <a href="{!! URL::route('approver.enable',[$approver->id,2,1]) !!}" class="badge badge-danger"> <i class="far fa-check-circle"></i> Reject</a>
                            @else
                                <a href="{!! URL::route('approver.enable',[$approver->id,2,0]) !!}" class="badge badge-secondary"> <i class="far fa-times-circle"></i> Reject</a>
                            @endif
                            @if ($approver->is_verified == 1)
                                <a href="{!! URL::route('approver.enable',[$approver->id,3,1]) !!}" class="badge badge-info"> <i class="far fa-check-circle"></i> Verify</a>
                            @else 
                                <a href="{!! URL::route('approver.enable',[$approver->id,3,0]) !!}" class="badge badge-secondary"> <i class="far fa-times-circle"></i> Verify</a>
                            @endif
                            @if ($approver->is_edit == 1)
                                <a href="{!! URL::route('approver.enable',[$approver->id,4,1]) !!}" class="badge badge-warning"> <i class="far fa-check-circle"></i> Edit</a>
                            @else 
                                <a href="{!! URL::route('approver.enable',[$approver->id,4,0]) !!}" class="badge badge-secondary"> <i class="far fa-times-circle"></i> Edit</a>
                            @endif
                            @if ($approver->is_image == 1)
                                <a href="{!! URL::route('approver.enable',[$approver->id,5,1]) !!}" class="badge badge-primary"> <i class="far fa-check-circle"></i> Image</a>
                            @else
                                <a href="{!! URL::route('approver.enable',[$approver->id,5,0]) !!}" class="badge badge-secondary"> <i class="far fa-times-circle"></i> Image</a>
                            @endif
                            @if ($approver->is_comment == 1)
                                <a href="{!! URL::route('approver.enable',[$approver->id,6,1]) !!}" class="badge badge-dark"> <i class="far fa-check-circle"></i> Comment</a>
                            @else 
                                <a href="{!! URL::route('approver.enable',[$approver->id,6,0]) !!}" class="badge badge-secondary"> <i class="far fa-times-circle"></i> Comment</a>
                            @endif

                            
                        </td>
                        <td><a href="{!! URL::route('approver.edit',$approver->id) !!}" class="btn btn-primary btn-sm"><i class="far fa-edit"></i> Edit</a></td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>