@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Approver</li>
    <li class="breadcrumb-item active">Add</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card mb-3">
            <div class="card-header">
            <i class="fas fa-user-check"></i>
            Add New Approver</div>
            <div class="card-body">
                {!! Form::open(['route'=>'approver.save']) !!}
                    @include('approver.form',['submitText'=>'Save'])
                {!! Form::close() !!}
            </div>            
        </div>
    </div>
</div>
@include('approver.list')
@endsection