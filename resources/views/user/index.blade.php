@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Overview</li>
</ol>
@endsection

@section('content')
    <div class="card mb-3 border-success">
        <h5 class="card-header bg-success text-white">Request Status</h5>
        <div class="card-body">
            @include('user._request')
        </div>
    </div>
    @foreach($approver_assignments as $assignment)    
    <div class="card mb-3 border-primary">
        <h5 class="card-header bg-primary text-white">
            {!! $assignment->approverseq->approver->approver !!}
        </h5>
        <div class="card-body">
            <table class="table table-bordered">                
                <tr class="bg-info text-white">
                    <th>SN</th>
                    <th>Type</th>                    
                    <th>Article / Post</th>
                    <th>Status / Remarks</th>                    
                </tr>
                @php 
                    $sn = 1;
                @endphp
                @foreach($assignment->approverseq->postapprovals as $postapproval)
                    @if($postapproval->approver_id == -1 && $postapproval->post->req_media_status_id ==  2)
                    <tr>
                        <td>{!! $sn++ !!}</td>
                        <td>{!! $postapproval->post->posttype->post_type !!}</td>                        
                        <td>
                            @if ($postapproval->approverseq->approver->is_assign == 1) 
                                <p><strong>Title :</strong> {!! $postapproval->post->title !!}</p>
                                <p><strong>Content :</strong> {!! $postapproval->post->content !!}</p>
                                {{-- TODO: display here list of proofreaders in department prooceed to current list or delete the current proof reader and assign new proof reader --}}
                                <small>
                                <table>
                                    <tr>
                                        <th>Proof Reader</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($proofreaders as $pr)
                                        @if ($pr->req_media_post_type_id == $postapproval->req_media_post_type_id)
                                            @foreach($pr->assignments as $prassigment)
                                            <tr>
                                                <td>{!! $prassignment->user->fullname !!}</td>
                                                <td>
                                                    <a href="{!! URL::route('post.approval',[$postapproval->req_media_post_id,$postapproval->id]) !!}" class="btn btn-sm btn-success" title="Send For Proofreading"> <i class="far fa-thumbs-up"></i> </a>                                        
                                                    <a href="{!! URL::route('post.deleteproofreader',$prassignment->id) !!}" class="btn btn-sm btn-danger" title="delete Assigned Proof Reader"><i class="far fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    
                                    @endforeach
                                </table>
                                
                                {!! Form::open(['route'=>['post.assignproofreader',$postapproval->req_media_post_id,$postapproval->id]]) !!}
                                    <div class="form-row align-items-center">
                                        <div class="col-auto">
                                            {!! Form::select('proofreader',$users, null, ['class'=>'form-control','placeholder'=>'Select Proof Reader']) !!}    
                                        </div>
                                        <div class="col-auto">
                                            <button href="#" class="btn btn-sm btn-primary">Assign New Proof Reader</button>    
                                        </div>                                    
                                    </div>
                                {!! Form::close() !!}
                                </small>
                            @else
                            {!! Form::open(['route'=>['post.verify',$postapproval->post->id]]) !!}
                                <div class="form-group">
                                    <label for="title" class="control-label">Title</label>
                                    {!! Form::text('title',$postapproval->post->title,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                <label for="content" class="control-label"> Content</label>
                                    {!! Form::textarea('content', $postapproval->post->content) !!}
                                </div>                                
                                    @if ($postapproval->approverseq->approver->is_edit == 1)
                                        <button class="btn btn-sm btn-info"><i class="far fa-edit"></i> Update/Save </button> 
                                    @endif
                                    @if ($postapproval->approverseq->approver->is_approved == 1)
                                        <a href="{!! URL::route('post.approval',[$postapproval->req_media_post_id,$postapproval->id]) !!}" class="btn btn-sm btn-success"> <i class="far fa-thumbs-up"></i> Send For Posting</a>
                                    @endif
                                    @if ($postapproval->approverseq->approver->is_reject == 1)
                                        <a data-toggle="collapse" aria-expanded="false" href="#reject{!! $postapproval->post->id !!}" class="btn btn-sm btn-danger"> <i class="far fa-thumbs-down"></i> Reject</a>
                                    @endif
                                    @if ($postapproval->approverseq->approver->is_image == 1)                                
                                        <a data-toggle="collapse" aria-expanded="false" href="#filed{!! $postapproval->post->id !!}" class="btn btn-sm btn-warning"><i class="far fa-images"></i> Images </a> 
                                    @endif
                                    
                                    @if ($postapproval->approverseq->approver->is_comment == 1) {{-- Not yet implemented --}}
                                    <a data-toggle="collapse" aria-expanded="false" href="#comment{!! $postapproval->post->id !!}" class="btn btn-sm btn-primary"><i class="far fa-edit"></i> Comment</a> 
                                    @endif
                            {!! Form::close() !!}
                            @endif
                            
                        </td>
                        <td>
                            
                        {{-- $postapproval->status->description --}} - {!! $postapproval->comment !!}</td>                        
                    </tr>
                    <tr class="collapse" id="reject{!! $postapproval->post->id !!}">
                        <td colspan="4">
                            {!! Form::open(['route'=>['post.reject',$postapproval->req_media_post_id, $postapproval->id]]) !!}
                                <div class="form-group">
                                    <label for="rejectlbl" class="control-label">Remarks (Reject)</label>
                                    {!! Form::text('comment',null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-sm btn-danger"><i class="far fa-thumbs-down"></i> Reject Now</button>
                                </div>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    <tr class="collapse" id="filed{!! $postapproval->post->id !!}">
                        <td colspan="4">
                            <div class="card-deck">
                                @foreach($postapproval->post->files as $file)
                                    <div class="card">
                                        <div class="card-header">
                                            Image-Ref-No-{!! $file->id !!}
                                        </div>
                                        <img src="{!! asset('storage/'.$file->file_path) !!}" >
                                    </div>
                                @endforeach
                            </div>
                        </td>
                    </tr>
                    
                    @endif
                @endforeach
            </table>
        </div>
    </div>
    @endforeach

@endsection

@section('jscript')
<script>
    tinymce.init({
        selector:'textarea',
        menubar: false   
    });
</script>


<script>    
    $('.deletebtn').click(function(e){
        e.preventDefault();
        var href = $(this).data('link');
        Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {                    
                    if (result.value) {
                        document.location.href = href;
                    }
                });      
    });        
            
</script>

@endsection