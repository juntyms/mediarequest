<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Media Request Login</title>

  <!-- Custom fonts for this template-->
  <link href="{!! URL::asset('sbadmin/vendor/fontawesome-free/css/all.min.css') !!}" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="{!! URL::asset('sbadmin/css/sb-admin.css') !!}" rel="stylesheet">

</head>

<body class="bg-dark">

  <div class="container">
    <div class="card card-login border-warning mx-auto mt-5">
      <div class="card-header bg-warning text-white">
        <h3>Login Page</h3>
      </div>
      <div class="card-body">        
        {!! Form::open(['route'=>'postlogin']) !!}
          <div class="input-group mb-3">            
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon2"><i class="fas fa-user-lock"></i></span>
            </div>
            <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username (e.g. john.doe)" required="required">           
                           
          </div>                      
          <div class="input-group mb-3">            
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon2"><i class="fas fa-key"></i></span>
            </div>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="required">                          
          </div>
          <button class="btn btn-primary btn-block">Login</button>        
        {!! Form::close() !!}
        
        
      </div>
      <div class="card-footer">
        <div class="text-center">
          <a class="d-block small" href="#">Copyright &copy 2019</a>
          <a class="d-block small" href="#">ETC - Salalah College of Technology</a>
        </div>
      </div>
    </div>
  </div>
</body>

</html>
