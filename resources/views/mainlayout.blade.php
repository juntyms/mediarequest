<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Media Request Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="{!! URL::asset('sbadmin/vendor/fontawesome-free/css/all.min.css') !!}" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="{!! URL::asset('sbadmin/vendor/datatables/dataTables.bootstrap4.css') !!}" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{!! URL::asset('sbadmin/css/sb-admin.css') !!}" rel="stylesheet">

  <link href="{!! URL::asset('select2/css/select2.min.css') !!}" rel="stylesheet">

</head>

<body id="page-top">

    @include('partial.top')
  

  <div id="wrapper">

    <!-- Sidebar -->
    @include('partial.sidebar')
    
    
    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->        
        @yield('breadcrumb')
        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @include('flash::message')        
        @include('sweetalert::alert')


        @yield('content')


        <!-- Area Chart Example-->



      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      @include('partial.footer')
      
    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are your sure you want to logout?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Click "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{!! URL::route('logout') !!}">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{!! URL::asset('sbadmin/vendor/jquery/jquery.min.js') !!}"></script>
  <script src="{!! URL::asset('sbadmin/vendor/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{!! URL::asset('sbadmin/vendor/jquery-easing/jquery.easing.min.js') !!}"></script>

  <!-- Page level plugin JavaScript-->
  <script src="{!! URL::asset('sbadmin/vendor/chart.js/Chart.min.js') !!}"></script>
  <script src="{!! URL::asset('sbadmin/vendor/datatables/jquery.dataTables.js') !!}"></script>
  <script src="{!! URL::asset('sbadmin/vendor/datatables/dataTables.bootstrap4.js') !!}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{!! URL::asset('sbadmin/js/sb-admin.min.js') !!}"></script>

  <!-- Demo scripts for this page-->
  <script src="{!! URL::asset('sbadmin/js/demo/datatables-demo.js') !!}"></script>
  <script src="{!! URL::asset('sbadmin/js/demo/chart-area-demo.js') !!}"></script>
  <script src="{!! URL::asset('js/app.js') !!}"></script>

  <script src="{!! URL::asset('vendor/tinymce/tinymce.min.js') !!}"></script>
  <script src="{!! URL::asset('select2/js/select2.min.js') !!}"></script>
  @yield('jscript')
</body>

</html>
