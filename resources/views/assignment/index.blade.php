@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Approver Assignment</li>
    <li class="breadcrumb-item active"> List</li>
</ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card mb-3">
                <div class="card-header">Assign User</div>
                <div class="card-body">
                    {!! Form::open(['route'=>'assignment.add']) !!}
                        <div class="form-group">
                            {!! Form::select('deptid', $depts, null, ['class'=>'form-control','placeholder'=>'Select Department']) !!}
                        </div>
                        <button class="btn btn-primary btn-block"> Load</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    Approver Assignment List
                </div>                
                <div class="card-body">
                    
                    <table class="table table-bordered">
                        <tr>
                            <th>SN</th>
                            <th>Department</th>
                            <th>Media Type</th>
                            <th>Approver Type</th>                            
                            <th>User</th>
                            <th>Sequence</th>
                            <th>Action</th>
                        </tr>
                        @php 
                            $sn = 1;
                        @endphp
                        @foreach($approver_assignments as $app_assignment)
                        <tr>
                            <td>{!! $sn++ !!}</td>
                            <td>{!! $app_assignment->approverseq->department->name !!}</td>           
                            <td>{!! $app_assignment->approverseq->posttype->post_type !!}</td>                 
                            <td>{!! $app_assignment->approverseq->approver->approver !!}</td>
                            <td>{!! $app_assignment->user->fullname !!}</td>
                            <td>{!! $app_assignment->approverseq->seq !!}</td>
                            <td>                                
                                <a href="{!! route('assignment.delete',$app_assignment->id) !!}" class="badge badge-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    
@endsection