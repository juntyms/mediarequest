@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Assignment</li>
    <li class="breadcrumb-item active">add</li>
</ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    Assignment for  {!! $department->name !!}
                </div>
                <div class="card-body">
                    {!! Form::open(['route'=>['assignment.save',$department->id]]) !!}
                        <div class="form-group">
                            <label for="approver">Approver</label>
                            {!! Form::select('req_media_approver_seq_id',$approver_types, null, ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label for="user">User</label>
                            {!! Form::select('user_id',$users, null, ['class'=>'form-control','placeholder'=>'Select User','id'=>'assigapprover']) !!}
                        </div>
                        <button class="btn btn-primary ">Assign</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jscript')
<script>
    $(document).ready(function() {
        $('#assigapprover').select2();
    });
</script>
@endsection