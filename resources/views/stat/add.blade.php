@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Status</li>
    <li class="breadcrumb-item active">Add</li>
</ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    Add New Status
                </div>
                <div class="card-body">
                    {!! Form::open(['route'=>'stat.save']) !!}
                        <div class="form-group">
                            <label for="description">Description</label>
                            {!! Form::text('description',null, ['class'=>'form-control']) !!}
                        </div>
                        <button class="btn btn-primary ">Save</button>
                        <a href="{{ route('stat.index') }}" class="btn btn-secondary"> Cancel</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection