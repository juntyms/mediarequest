@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Status</li>
    <li class="breadcrumb-item active">Edit</li>
</ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    Edit Status
                </div>
                <div class="card-body">
                    {!! Form::model($stat,['route'=>['stat.update',$stat->id]]) !!}
                        <div class="form-group">
                            <label for="description">Description</label>
                            {!! Form::text('description',null, ['class'=>'form-control']) !!}
                        </div>
                        <button class="btn btn-primary "> Update</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection