@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Status</li>
    <li class="breadcrumb-item active">List</li>
</ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    List Status
                </div>
                <div class="card-body">
                    <a href="{{ route('stat.add') }}" class="btn btn-primary"> Add </a>
                    <hr />
                    <table class="table table-bordered">
                        <tr>
                            <th>SN</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                        @php 
                            $sn=1;
                        @endphp
                        @foreach($status as $stat)
                        <tr>
                            <td>{!! $sn++ !!}</td>
                            <td>{!! $stat->description !!}</td>
                            <td><a href="{!! URL::route('stat.edit',$stat->id) !!}">Edit</a></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection