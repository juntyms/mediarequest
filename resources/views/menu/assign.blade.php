@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>    
    <li class="breadcrumb-item active"> Assign</li>
    <li class="breadcrumb-item"> Menus</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                Assign User to Menu
            </div>
            <div class="card-body">
                {!! Form::open(['route'=>'menu.saveassign']) !!}
                <div class="form-group">
                    <label for="menus">Menu</label>
                    {!! Form::select('req_media_menu_id',$menus, null, ['class'=>'form-control','placeholder'=>'Select Menu']) !!}
                </div>
                <div class="form-group">
                    <label for="users">User</label>
                    {!! Form::select('user_id',$users, null, ['class'=>'form-control','placeholder'=>'Select User','id'=>'assignmenuuser']) !!}
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        
    </div>
</div>
@endsection

@section('jscript')
<script>
    $(document).ready(function() {
        $('#assignmenuuser').select2();
    });
</script>
@endsection