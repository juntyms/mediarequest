@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>    
    <li class="breadcrumb-item active"> Users</li>
    <li class="breadcrumb-item"> Menus</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                User Menus
            </div>
            <div class="card-body">   
                <a href="{!! URL::route('menu.assign') !!}" class="btn btn-primary">Assign</a>    
                <hr />         
                    @foreach($users as $user)
                    
                        <div class="card mb-3">
                            <div class="card-header">
                                {!! $user->fullname !!}
                            </div>
                            <div class="card-body">
                                
                                    @foreach($user->menus as $menu)                                            
                                        <a href="{!! URL::route('menu.delete',$menu->id) !!}" class="badge badge-pill badge-primary">x {!! $menu->menu->menu !!}</a>
                                    @endforeach
                                
                            </div>
                        </div>            
                             
                    @endforeach                
            </div>
        </div>
    </div>
</div>
@endsection