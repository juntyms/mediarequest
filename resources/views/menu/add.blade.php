@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>    
    <li class="breadcrumb-item active"> Add New</li>
    <li class="breadcrumb-item"> Menus</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                Add New Menu
            </div>
            <div class="card-body">
                {!! Form::open(['route'=>'menu.save']) !!}
                    <div class="form-group">
                        <label for="Parent">Parent Menu</label>
                        {!! Form::select('parent_id',$parent_menus, null, ['class'=>'form-control','placeholder'=>'Select Parent Menu']) !!}
                    </div>
                    <div class="form-group">
                        <label for="menu">Menu Name</label>
                        {!! Form::text('menu',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="url">Menu URL</label>
                        {!! Form::text('url',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="icon">Icon class</label>
                        {!! Form::text('icon',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"> Save</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection