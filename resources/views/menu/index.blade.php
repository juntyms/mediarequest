@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>    
    <li class="breadcrumb-item active"> Menus</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                Menus
            </div>
            <div class="card-body">
                <a class="btn btn-primary" href="{!! URL::route('menu.add') !!}">Add New Menu</a>         
                <hr />
                <table class="table table-bordered">
                    <tr>
                        <th>SN</th>
                        <th>Parent</th>
                        <th>Menu</th>
                        <th>URL</th>
                        <th>Icon Class</th>
                    </tr>
                    @foreach($menus as $menu)
                    <tr>
                        <td></td>
                        <td>{!! $menu->parent_id !!}</td>
                        <td>{!! $menu->menu !!}</td>
                        <td>{!! $menu->url !!}</td>
                        <td>{!! $menu->icon !!}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>

    </div>
</div>
@endsection