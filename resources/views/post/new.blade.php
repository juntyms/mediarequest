@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Post</li>
    <li class="breadcrumb-item active"> New Request</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-primary text-white">Request Post</div>
            <div class="card-body">
                {!! Form::open(['route'=>'post.save','files'=>'true']) !!}
                    @include('post.form',['submitButton'=>' Save'])                    
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>
@endsection

@section('jscript')
<script>
    tinymce.init({
        selector:'textarea',
        menubar: false   
    });
</script>
@endsection