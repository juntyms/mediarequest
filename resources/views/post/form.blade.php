<div class="form-group">
    <label for="post_type">Type</label>
    {!! Form::select('req_media_post_type_id',$reqtypes, null, ['class'=>'form-control col-md-4','placeholder'=>'Media Type']) !!}
</div>
<div class="form-group">
    <label for="title">Title</label>
    {!! Form::text('title',null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    <label for="content">Content</label>                                                
    {!! Form::textarea('content',null, ['class'=>'description']) !!}
</div>
@if ($submitButton != "Update")
<div class="form-group">
    {!! Form::file('image') !!}
</div>
@endif
<div class="form-group">
    <button class="btn btn-primary"> {{ $submitButton }}</button>
</div>