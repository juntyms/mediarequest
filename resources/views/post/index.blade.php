@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Post</li>
    <li class="breadcrumb-item active"> All Request</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header">All Requests</div>
        <div class="card-body">
            <small>
            <table class="table table-bordered">
            <tr class="bg-dark text-white">
                <th>SN</th>
                <th>Request Type</th>
                <th>Title</th>
                <th>Content</th>
                <th>File Attachment</th>
                <th class="text-center">Status</th>
                <th>Action</th>
            </tr>
            @php 
                $sn = 1;
            @endphp
            @foreach($reqs as $req)
                <tr>
                    <td>{!! $sn++ !!}</td>
                    <td>{!! $req->posttype->post_type !!}</td>
                    <td>{!! $req->title !!}</td>
                    <td>{!! $req->content !!}</td>
                    <td>                        
                        <a data-toggle="collapse" aria-expanded="false" href="#file{!! $req->id !!}">View Files ( {!! $req->files->count() !!} )</a>                         
                    </td>
                    <td class="text-center">{!! $req->status->description !!}</td>
                    <td>
                        @if ($req->req_media_status_id == 1)
                        {!! Form::open(['route'=>['post.status',$req->id]]) !!}
                            {!! Form::hidden('status',2) !!}
                            <button class="btn btn-primary btn-sm"> Submit</button>
                        {!! Form::close() !!}
                        <a href="{!! URL::route('post.edit',$req->id) !!}" class="btn btn-sm btn-warning"> Edit</a>
                        @endif
                    </td>
                </tr>
                
                <tr class="collapse" id="file{!! $req->id !!}">
                    <td colspan="6">
                    @if ($req->req_media_status_id == 1)
                        {!! Form::open(['route'=>['post.addfile',$req->id],'files'=>'true']) !!}
                            {!! Form::file('image') !!}
                            <button> Upload</button>
                        {!! Form::close() !!}
                    @endif
                        <hr />                    
                            <div class="card-deck">
                            @foreach($req->files as $file)                            
                                <div class="card">                                
                                    <img src="{!! asset('storage/'.$file->file_path) !!}" class="card-img-top">                                
                                    <div class="card-footer">
                                    @if ($req->req_media_status_id == 1)
                                    <a href="#" data-id="{!! $file->id !!}" data-link="{!! URL::route('post.deletefile',$file->id) !!}" class="deletebtn"><button><i class="fas fa-trash-alt"></i> Delete </button></a>
                                    @endif
                                    </div>
                                </div>                                                          
                            @endforeach                 
                            </div>       
                    </td>
                </tr>                                
            @endforeach
            </table>
            </small>
        </div>
        </div>
    </div>
</div>
@endsection

@section('jscript')
<script>    
    $('.deletebtn').click(function(e){
        e.preventDefault();
        var href = $(this).data('link');
        Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {                    
                    if (result.value) {
                        document.location.href = href;
                    }
                });      
    });        
            
</script>
@endsection