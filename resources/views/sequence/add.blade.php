@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Approver Sequence</li>
    <li class="breadcrumb-item active"> Assignment</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card mb-3">
            <div class="card-header">
            <i class="fas fa-user-check"></i>
            Add New Approver Sequence</div>
            <div class="card-body">
               {!! Form::open(['route'=>'seq.save']) !!}
                    <div class="form-group">
                        <label for="Type">Type</label>
                        {!! Form::select('req_media_post_type_id',$types, null, ['class'=>'form-control','placeholder'=>'Select Type']) !!}
                    </div>
                    <div class="form-group">
                        <label for="Approver">Approver</label>
                        {!! Form::select('req_media_approver_id', $approvers, null, ['class'=>'form-control','placeholder'=>'Select Approver']) !!}
                    </div>
                    <div class="form-group">
                        <label for="dept">Department</label>
                        {!! Form::select('dept_id',$depts, null, ['class'=>'form-control','placeholder'=>'Select Department']) !!}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-sm"> <i class="fas fa-save"></i> Save </button>
                    </div>
               {!! Form::close() !!}
            </div>            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        @foreach($seqs as $seq)
            <div class="card mb-3">
                <div class="card-header bg-primary text-white">
                    {!! $seq->name !!}    
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                    <tr class="bg-info text-white">
                        <th>SN</th>
                        <th>Approver</th>
                        <th>Type</th>
                        <th>Seq</th>
                    </tr>
                    @php 
                        $sn = 1;
                    @endphp
                    @foreach($seq->seqs as $s)
                        <tr>
                            <td>{!! $sn++ !!}</td>
                            <td>{!! $s->approver->approver !!}</td>
                            <td>{!! $s->posttype->post_type !!}</td>
                            <td>{!! $s->seq !!}</td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection