@php
$menu1 = 0;
$menu2 = 0;
$menu3 = 0;
$menu4 = 0;
$menu5 = 0;
$menu6 = 0;
$menu7 = 0;
$menu8 = 0;
$menu9 = 0;
$menu10 = 0;
$menu11 = 0;
$menu12 = 0;
$menu13 = 0;
$menu14 = 0;
$menu15 = 0;
$menu16 = 0;
$menu17 = 0;

foreach($usermenus as $menu) {
    switch ($menu->req_media_menu_id) {
        case 1:
            $menu1 = 1;
            break;
        case 2:
            $menu2 = 1;
            break;
        case 3:
            $menu3 = 1;
            break;
        case 4:
            $menu4 = 1;
            break;
        case 5:
            $menu5 = 1;
            break;
        case 6:
            $menu6 = 1;
            break;
        case 7:
            $menu7 = 1;
            break;
        case 8:
            $menu8 = 1;
            break;
        case 9:
            $menu9 = 1;
            break;
        case 10:
            $menu10 = 1;
            break;
        case 11:
            $menu11 = 1;
            break;
        case 12:
            $menu12 = 1;
            break;
        case 13:
            $menu13 = 1;
            break;
        case 14:
            $menu14 = 1;
            break;
        case 15:
            $menu15 = 1;
            break;
        case 16:
            $menu16 = 1;
            break;
        case 17:
            $menu17 = 1;
            break;
    }
}

@endphp
<ul class="sidebar navbar-nav">
    <li class="nav-item active">
    <a class="nav-link" href="{!! URL::route('index') !!}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
    </a>
    </li>    

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-paste"></i>
            <span>My Requests</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Request Options:</h6>
            <a class="dropdown-item" href="{!! URL::route('post.new') !!}">New</a>
            <a class="dropdown-item" href="{!! URL::route('post.index') !!}">Display all</a>            
        </div>
    </li>
    @if (($menu1 == 1) || ($menu2 == 1) || ($menu3 == 1))
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Types</span>
        </a>
        @if (($menu2 == 1) || ($menu3 == 1))
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Type Options:</h6>
            @if ($menu2 == 1)
            <a class="dropdown-item" href="{!! URL::route('type.add') !!}">Add New</a>
            @endif
            @if ($menu3 == 1)
            <a class="dropdown-item" href="{!! URL::route('type.index') !!}">Display all</a>     
            @endif       
        </div>
        @endif
    </li>
    @endif
    @if (($menu4 == 1) || ($menu5 == 1) || ($menu6 == 1)  || ($menu7 == 1) || ($menu8 == 1))
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-check"></i>
            <span>Approver</span>
        </a>
        @if (($menu5 == 1) || ($menu6 == 1)  || ($menu7 == 1) || ($menu8 == 1))
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Approvers Options:</h6>
            @if ($menu5 == 1)            
            <a class="dropdown-item" href="{!! URL::route('approver.add') !!}">Add New Approver</a>
            @endif
            @if ($menu6 == 1)            
            <a class="dropdown-item" href="{!! URL::route('approver.index') !!}">Display all Approver</a>            
            @endif
            @if ($menu7 == 1)            
            <a class="dropdown-item" href="{!! URL::route('seq.add') !!}">Approver Sequence</a>    
            @endif
            @if ($menu8 == 1)                    
            <a class="dropdown-item" href="{!! URL::route('assignment.index') !!}">Assignment list</a>     
            @endif 
        </div>
        @endif
    </li>
    @endif
    @if (($menu9 == 1) || ($menu10 == 1) || ($menu11 == 1))
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-exclamation-circle"></i>
            <span>Status</span>
        </a>
        @if (($menu10 == 1) || ($menu11 == 1))
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Status Options:</h6>
            @if ($menu10 == 1)
            <a class="dropdown-item" href="{!! URL::route('stat.index') !!}">List Status</a>
            @endif
            @if ($menu11 == 1)
            <a class="dropdown-item" href="{!! URL::route('stat.add') !!}">Add New Status</a>
            @endif
        </div>
        @endif
    </li>  
    @endif      
    @if (($menu12 == 1) || ($menu13 == 1) || ($menu14 == 1) || ($menu15 == 1) || ($menu16 == 1))
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-ellipsis-v"></i>
            <span>Menus</span>
        </a>
        @if (($menu13 == 1) || ($menu14 == 1) || ($menu15 == 1) || ($menu16 == 1))
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Menu Options:</h6>
            @if ($menu13 == 1)
            <a class="dropdown-item" href="{!! URL::route('menu.add') !!}">Add New Menu</a>   
            @endif         
            @if ($menu14 == 1)
            <a class="dropdown-item" href="{!! URL::route('menu.index') !!}">Display Menus</a>            
            @endif
            @if ($menu15 == 1)
            <a class="dropdown-item" href="{!! URL::route('menu.assign') !!}">Assign User</a>            
            @endif
            @if ($menu16 == 1)
            <a class="dropdown-item" href="{!! URL::route('menu.user') !!}">Menu Assignment</a>            
            @endif
        </div>
        @endif
    </li>
    @endif
    @if ($menu17 == 1)
    <li class="nav-item">
    <a class="nav-link" href="{!! URL::route('report.index') !!}">
        <i class="far fa-clipboard"></i>
        <span> Reports</span>
    </a>
    </li>
     @endif
</ul>