<footer class="sticky-footer">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
        <span>Copyright © 2019 | {{ config('app.name') }} | Educational Technology Center | Salalah College of Technology</span>
        </div>
    </div>
</footer>