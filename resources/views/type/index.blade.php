@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Type</li>
    <li class="breadcrumb-item active">List</li>
</ol>
@endsection

@section('content')
    @include('type.list')
@endsection