@extends('mainlayout')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item">Type</li>
    <li class="breadcrumb-item active">Add</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card mb-3">
            <div class="card-header">
            <i class="fas fa-chart-area"></i>
            Update Type</div>
            <div class="card-body">
                {!! Form::model($post_type,['route'=>['type.update',$post_type->id]]) !!}
                    <div class="form-group">
                        <label for="typename" class="control-label">Name</label>
                        {!! Form::text('post_type',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"> <i class="fas fa-save"></i> Update</button>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="card-footer small text-muted">&nbsp;</div>
        </div>
    </div>
</div>
@include('type.list')
@endsection