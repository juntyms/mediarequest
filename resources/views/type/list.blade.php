<div class="row">
    <div class="col-lg-12">
        <div class="card mb-3">
            <div class="card-header"> Type List</div>
            <div class="card-body">
                <table class="table table-bordered">
                    <tr class="thead-light">
                        <th>SN</th>
                        <th>Post Type</th>
                        <th>Action</th>
                    </tr>
                    @php 
                        $sn = 1;
                    @endphp
                    @foreach($req_media_post_types as $req_media_post_type)
                    <tr>
                        <td>{!! $sn++ !!}</td>
                        <td>{!! $req_media_post_type->post_type !!}</td>
                        <td><a href="{!! URL::route('type.edit',$req_media_post_type->id) !!}" class="btn btn-sm btn-primary"> <i class="far fa-edit"></i> Edit</a></td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>